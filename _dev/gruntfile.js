module.exports = function (grunt) {
    const package_name = 'bwg-vendor';

    // noinspection JSUnresolvedFunction
    grunt.initConfig({
        compress: {
            main: {
                options: {
                    archive: function () {
                        return '../' + package_name + '.zip';
                    },
                    mode: 'zip'
                },
                files: [
                    {
                        expand: true,
                        cwd: '../',
                        src: [
                            '**',
                            '!_dev/**',
                            '!assets/*.xcf',
                            '!.gitignore',
                            '!.gitlab-ci.yml',
                            '!' + package_name + '.zip',
                            '!composer.json',
                            '!composer.lock',
                            '!README.md',
                        ],
                        dest: package_name + '/'
                    }
                ]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-compress');

    // Register the tasks.
    grunt.registerTask('build', ['compress']);
};
