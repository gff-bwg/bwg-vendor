=== BWG Vendor ===
Contributors: masterida
Donate link: https://www.gff.ch/
Requires at least: 5.0
Tested up to: 6.0
Requires PHP: 7.4
Stable tag: 1.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Provides the BWG Vendor.

== Description ==

The **BWG Vendor** plugin provides some third-party libraries used by the BWG plugin.

== Changelog ==

= 1.1 =
* Minor improvements.

= 1.0 =
* Initial version.
