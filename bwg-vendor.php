<?php

/*
Plugin Name: BWG Vendor
Description: This plugin contains libraries that would be required by the BWG plugin.
Author: A. Suter
Author URI: http://www.gff.ch/
Text Domain: bwg-vendor
Version: 1.1
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/**
 * Absolute filesystem path to the bwg-vendor plugin root file.
 */
define( 'BWG_VENDOR_ABS_PLUGIN_FILE', __FILE__ );

/**
 * Absolute filesystem path to the bwg-vendor plugin.
 */
define( 'BWG_VENDOR_ABS_PATH', dirname( __FILE__ ) );

/**
 * The BWG Vendor Build Number (would only be increased if a library had been updated).
 */
define( 'BWG_VENDOR_BUILD_NUMBER', 2 );


///
// COMPOSER AUTOLOAD
///
include( BWG_VENDOR_ABS_PATH . '/vendor/autoload.php' );


///////////////////////////// PLUGIN UPDATE CHECKER ////////////////////////////


call_user_func( function () {
	$group_name     = 'gff-bwg';
	$plugin_name    = basename( __FILE__, '.php' );
	$update_checker = Puc_v4_Factory::buildUpdateChecker(
		'https://gitlab.com/' . $group_name . '/' . $plugin_name . '/',
		__FILE__
	);

	/** @var Puc_v4p11_Vcs_GitLabApi $vcs_api */
	$vcs_api = $update_checker->getVcsApi();
	$vcs_api->enableReleasePackages();
} );

//add_action( 'plugins_loaded', function () {
//	Puc_v4_Factory::buildUpdateChecker(
//		'http://wp-repository.gff.ch/bwg-vendor/metadata.json',
//		BWG_VENDOR_ABS_PLUGIN_FILE,
//		'bwg-vendor'
//	);
//} );
